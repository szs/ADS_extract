ADS_extract.py
==============

Extract infomation from the SAO/NASA Astrophysics Data System

usage:  ADS_extract.py `<author>`

author: Steffen Brinkmann <brinkmann@mailbox.org>

license: (C) 2015 published under the GNU GPL (https://gnu.org/licenses/gpl.html)
