#!/bin/env python
# -*- coding: utf-8 -*-

# ADS_extract.py
#
# extract infomation from the SAO/NASA Astrophysics Data System
#
# usage:  ADS_extract.py <author>
#
# author: Steffen Brinkmann <brinkmann@mailbox.org>
#
# license: (C) 2015 published under the GNU GPL (https://gnu.org/licenses/gpl.html)

from __future__ import print_function

try:
    from html.parser import HTMLParser
except:
    from HTMLParser import HTMLParser

import xml.etree.ElementTree as ET

import re
import sys
import os

only_refereed = True
parse_format = "XML"

def convert_month(month_short):
    if month_short == 'Jan':
        return 1
    elif month_short == 'Feb':
        return 2
    elif month_short == 'Mar':
        return 3
    elif month_short == 'Apr':
        return 4
    elif month_short == 'May':
        return 5
    elif month_short == 'Jun':
        return 6
    elif month_short == 'Jul':
        return 7
    elif month_short == 'Aug':
        return 8
    elif month_short == 'Sep':
        return 9
    elif month_short == 'Oct':
        return 10
    elif month_short == 'Nov':
        return 11
    elif month_short == 'Dec':
        return 12
    else:
        return 0

class Publication(object):

    def __init__(self,
                 bibcode="",
                 year=0,
                 month=0,
                 n_authors=0,
                 position=0,
                 authors=[],
                 title="",
                 journal=""):
        self.bibcode=bibcode
        self.year=year
        self.month=month
        self.n_authors=n_authors
        self.position=position
        self.authors=authors
        self.title=title
        self.journal=journal

class ADSHTMLParser(HTMLParser, object):
    def __init__(self):
        super(ADSHTMLParser,self).__init__()
        self._interesting = False
        self._next_row_interesting = False # when date is found, authors and title are in the next <tr>
        self._this_row_interesting = False
        self._right_table = False
        self._table_count = 0
        self._td_count = 0

    def handle_starttag(self, tag, attrs):
        #~ print("Encountered a start tag:", tag)
        if tag == 'table':
            self._table_count += 1
            if self._table_count == 3:
                self._right_table = True
            else:
                self._right_table = False

        if self._right_table == True:
            if tag == 'td':
                self._interesting = True
                self._td_count += 1

        if self._right_table == True:
            if tag == 'tr':
                self._td_count = 0


    def handle_endtag(self, tag):
        #~ print("Encountered an end tag :", tag)
        if tag == 'table' or tag == 'td':
            self._interesting = False
        if tag == 'tr':
            if self._next_row_interesting:
                self._this_row_interesting = True
            else:
                self._this_row_interesting = False
            self._next_row_interesting = False

    def handle_data(self, data):
        #~ print("Encountered some data  :", data)
        if ((len(data) > 1) and
            (not data.isspace()) and
            (self._interesting == True) ) :
            #~ print(data)
            if re.match("[0-9]{2}/[0-9]{4}",data):
                #~ print ("#### date: %s" % data)
                print ("%s-%s" % (data[3:],data[0:2]), end='\t')
                self._next_row_interesting = True
            if self._this_row_interesting:
                if self._td_count == 2:
                    author_list = data.split(";")
                    print (len(author_list), end='\t')
                    i = 1
                    for a in author_list:
                        #print (a)
                        if re.search(author_lastname, a.lower()):
                            break
                        i += 1
                    print (i)
                    #~ print (data.split(";"))
                    #~ print("### authors: %s" % data.split(";"))
                #~ elif self._td_count == 4:
                    #~ print("### title: %s" % data)

if parse_format == "HTML":
    parser = ADSHTMLParser()

if len(sys.argv) != 2:
    print("usage: ADS_extract.py <author>")
    sys.exit(1)

author = sys.argv[1]

author_string = author.lower()
author_string = author_string.replace(" ","+")
author_lastname = author_string.split(',')[0]

print ("# ", author_lastname, author_string)

filename = author
filename = filename.replace(" ","")
filename = filename.replace(",","")
filename = filename.replace(".","")

if parse_format == "XML":
    filename = filename+".xml"
if parse_format == "HTML":
    filename = filename+".html"

cmd = "wget -O %s http://esoads.eso.org/cgi-bin/abs_connect?author=%s\&nr_to_return=1000" % (filename, author_string)

if parse_format == "XML":
    cmd += "\&data_type=SHORT_XML"

if only_refereed == True:
    cmd += "\&jou_pick=NO"

#print (cmd)
os.system (cmd)

with open(filename, 'r') as f:
    text = f.read()
    text = text.replace('&#160;',' ')
    text = text.replace('&#241;','ñ')
    text = text.replace('&#225;','á')
    text = text.replace('&#233;','é')
    text = text.replace('&#237;','í')
    text = text.replace('&#243;','ó')
    text = text.replace('&#250;','ú')
    text = text.replace('&#193;','Á')
    text = text.replace('&#201;','É')
    text = text.replace('&#205;','Í')
    text = text.replace('&#211;','Ó')
    text = text.replace('&#218;','Ú')
    text = text.replace('&#228;','ä')
    text = text.replace('&#246;','ö')
    text = text.replace('&#252;','ü')
    text = text.replace('&#196;','Ä')
    text = text.replace('&#214;','Ö')
    text = text.replace('&#220;','Ü')

    # print ("# date\tn_authors\tposition")
    print ("# year\tpublications")

    if parse_format == "HTML":
        parser.feed(text)
    elif parse_format == "XML":
        publications = []
        min_year = 9999
        max_year = 0
        root = ET.fromstring(text)
        ADS_namespace = re.search('{(.*)}',root.tag).group(0)
        #ns = {'ADS': ADS_namespace} # for Python >=2.7
        print ("# %i refereed publications" % len(root.findall(ADS_namespace+"record")))
        for rec in root.findall(ADS_namespace+"record"):
            year = int(rec.find(ADS_namespace+"pubdate").text[-4:])
            if year > max_year: max_year = year
            if year < min_year: min_year = year

            authors = rec.findall(ADS_namespace+"author")
            n_authors = len(authors)
            position = 1
            for author in authors:
                if author_lastname in author.text.lower():
                    break
                position += 1
            #print (year, n_authors, position)
            publications.append(Publication(bibcode=rec.find(ADS_namespace+"bibcode").text,
                                            month=convert_month(rec.find(ADS_namespace+"pubdate").text[:3]),
                                            year=year,
                                            n_authors=n_authors,
                                            position=position,
                                            authors=authors,
                                            title=rec.find(ADS_namespace+"title").text,
                                            journal=rec.find(ADS_namespace+"journal").text
                                            ))
        # count publications per year
        pub_per_year = {}
        for pub in publications:
            if pub.year in pub_per_year:
                pub_per_year[pub.year] += 1
            else:
                pub_per_year[pub.year] = 1

        for year in range(min_year,max_year+1):
            if year in pub_per_year:
                print (year, pub_per_year[year])
            else:
                print (year,0)
